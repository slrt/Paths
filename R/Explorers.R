.defaultPathCat <- function(parent, node) {
    if (node == "") {
        node <- NULL
    }
    if (parent == "") {
        parent <- NULL
    }
    return(base::gsub(pattern = .DOUBLE_SLASH, replacement = .SLASH, base::paste0(c(parent, node), collapse = .SLASH)))
}

.functionList <- list(
    "list.files" = list.files,
    "list.dirs" = list.dirs,
    "file.exists" = file.exists,
    "dir.exists" = dir.exists,
    "is.dir" = function(x) file.info(x)$isdir,
    "is.file" = function(x) !file.info(x)$isdir
)

#' Explorer
#'
#' Helper for paths. See \code{\link{Explorers$new}}.
#'
#' @export Explorers
#' @name Explorers
#' @rdname Explorers
Explorers <- (function() {
    ret0 <- list()

    ret0$new <- function(
        tree,
        parent = "",
        aliasRef = "name",
        process = .defaultPathCat,
        functions = .functionList
    ) {
        if (base::length(tree) == 0) {
            return(NULL)
        }

        returned <- list()

        .get <- function(key) {
            .key <- key
            function(suffix = NULL) {
                if (is.null(suffix)) {
                    return(.key)
                }
                return(process(.key, suffix))
            }
        }

        .func <- function(name, key) {
            .name <- name
            .key <- key
            function(suffix = NULL) {
                if (is.null(suffix)) {
                    return(functions[[.name]](.key))
                }
                return(functions[[.name]](process(.key, suffix)))
            }
        }

        .names <- names(tree)
        .names <- .names[.names != aliasRef]

        for (kn in .names) {
            if (is.null(tree[[kn]][[aliasRef]])) {
                val <- kn
            } else {
                val <- tree[[kn]][[aliasRef]]
            }
            key <- process(parent, val)
            cn <- base::Recall(tree[[kn]], key, aliasRef, process)
            if (is.null(cn)) {
                returned[[kn]] <- list()
            } else {
                returned[[kn]] <- cn
            }
            returned[[kn]][["get"]] <- .get(key)
            # returned[[kn]][["attach"]] <- function(key, value = NULL) {
            #
            # }
            for (fn in names(functions)) {
                returned[[kn]][[fn]] <- .func(fn, key)
            }
        }

        base::rm(.names)

        return(returned)
    }

    return(ret0)
})()
#' resolve (see \code{\link{Explorers}})
#'
#' Yields a navigatable object with the structure of a tree. Each node is furnished with a predefined list of functions that operate on the node. For instance, \code{get} yields the concatenation of the tokens from the path leading from root to the current node.
#'
#' @param tree is a list denoting a tree, which can have one of the two following forms:\cr
#' \enumerate{
#'     \item \preformatted{e <- list(
#'  <root> = list(
#'    <level1, element1> = list(),
#'    <level1, element2> = list(),
#'    …
#'  )
#')} yields a tree where each node has a name that is it's value. E.g. \code{e$<root>$get()} yields \code{<root>}, while \code{e$<root>$<level1, element1>$get()} yields \code{<root>/<level1, element1>}.
#'     \item \preformatted{list(
#'   <root> = list(
#'     "name" = <root value>,
#'     <level1, element1> = list(
#'       "name" = <level1, element1 value>
#'     ),
#'     <level1, element2> = list(
#'       "name" = <level1, element2 value>
#'     )
#'   )
#' )} yields a tree where each node has a name. For instance \code{e$<root>$get()} yields \code{<root value>}, while \code{e$<root>$<level1, element1>$get()} yields \code{<root>/<level1, element1 value>}. \code{"name"} is the default value for \code{aliasRef}.
#' }
#' @param parent value of the token that must be added before the value of \code{…$get()}. Default is \code{""}.
#' @param aliasRef the name of the key for node values. Default is \code{"name"}.
#' @param process function that processes a node, and the result of it's parent's processing. Default concatenates tokens to form a valid path.
#' @param functions functions available at each node's level.
#' @return an object denoting the concatenation of this path and the concatenation of token given as parameters
#' @examples
#' dir <- Explorers$new(
#'     tree = list(
#'         "root" = list(
#'             "name" = "home",
#'             "first_user" = list(
#'                 "name" = "dany"
#'             ),
#'             "second_user" = list(
#'                 "name" = "frank"
#'             )
#'         )
#'     ),
#'     parent = "/",
#'     aliasRef = "name"
#' )
#' dir$root$get()
#' # [1] "/home"
#' dir$root$first_user$get()
#' # [1] "/home/dany"
# dir$root$second_user$get()
#' # [1] "/home/frank"
#' @name Explorers$new
NULL
# Explorers
# Explorers$new(list("root"))
# dir <- Explorers$new(
#     tree = list(
#         "root" = list(
#             "name" = "home",
#             "first_user" = list(
#                 "name" = "dany"
#             ),
#             "second_user" = list(
#                 "name" = "frank"
#             )
#         )
#     ),
#     parent = "/",
#     aliasRef = "name"
# )
